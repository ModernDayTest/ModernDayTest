﻿capital = 266

oob = "PER_2017"

set_research_slots = 3
set_convoys = 75
set_stability = 0.25

set_country_flag = country_language_persian

set_technology = {

}

add_ideas = {

}

set_politics = {

	parties = {
	    islamist = {
	        popularity = 77
	    }
		
	    fascist = {
	        popularity = 0
	    }
		
	    nationalist = {
	        popularity = 0
	    }
		
		monarchist = {
	        popularity = 0
	    }
		
	    reactionary = {
	        popularity = 0
	    }
		
	    conservative = {
	        popularity = 15
	    }
		
	    market_liberal = {
	        popularity = 2
	    }
		
	    social_liberal = {
	        popularity = 1
	    }
		
	    social_democrat = {
	        popularity = 0
	    }
		
	    progressive = {
	        popularity = 0
	    }
		
	    democratic_socialist = {
	        popularity = 5
	    }
		
	    communist = {
	        popularity = 0
	    }
	}
	
	ruling_party = islamist
	last_election = "2017.5.19"
	election_frequency = 48
	elections_allowed = no
}

create_country_leader = {
	name = "Ali Khamenei"
	ideology = islamic_authoritarian
	picture = "MDT_Ali_Khamenei.dds"
}

create_field_marshal = {
	name = "Ahmadreza Pourdastan"
		picture = "Field_Marshall_Army_Ahmadreza_Pourdastan.dds"
	traits = { defensive_doctrine }
	skill = 3

}

create_field_marshal = {
	name = "Mohammed Bagheri"
		picture = "IRCG_Field_Marshall_Mohammed_Bagheri.dds"
	traits = { inspirational_leader }
	skill = 2

}

create_corps_commander = {
	name = "Ali Hajilo"
	picture = "Army_Gen_Ali_Hajilo.dds"
	traits = { desert_fox trait_mountaineer }
	skill = 3
}

create_corps_commander = {
	name = "Hassan Saadi"
	picture = "Army_Gen_Hassan_Saadi.dds"
	traits = { panzer_leader desert_fox }
	skill = 4
}

create_corps_commander = {
	name = "Heshmatollah Malekian"
	picture = "Army_Gen_Heshmatollah_Malekian.dds"
	traits = { trait_mountaineer }
	skill = 1
}

create_corps_commander = {
	name = "Kiomars Sharafi"
	picture = "Army_Gen_Kiomars_Sharafi.dds"
	traits = { trait_mountaineer }
	skill = 1
}

create_corps_commander = {
	name = "Manoucher Kazemi"
	picture = "Army_Gen_Manoucher_Kazemi.dds"
	traits = { hill_fighter desert_fox }
	skill = 2
}

create_corps_commander = {
	name = "Nader Najafi"
	picture = "Army_Gen_Nader_Najafi.dds"
	traits = { panzer_leader }
	skill = 3
}

create_corps_commander = {
	name = "Abdol-Ali Najafi"
	picture = "IRCG_Gen_Abdol-Ali_Najafi.dds"
	traits = { commando desert_fox }
	skill = 1
}

create_corps_commander = {
	name = "Ali Shadmani"
	picture = "IRCG_Gen_Ali Shadmani.dds"
	traits = { commando hill_fighter }
	skill = 2
}

create_corps_commander = {
	name = "Mohammad Pakpour"
	picture = "IRCG_Gen_Mohammad_Pakpour.dds"
	traits = { commando desert_fox hill_fighter }
	skill = 4
}

create_corps_commander = {
	name = "Ali Jafari"
	picture = "IRCG_Gen_Mohammed_Ali_Jafari.dds"
	traits = { commando urban_assault_specialist }
	skill = 3
}

create_corps_commander = {
	name = "Mohammed-Nazer Azimi"
	picture = "IRCG_Gen_Mohammed-Nazer_Azimi.dds"
	traits = { commando trickster }
	skill = 2
}

create_corps_commander = {
	name = "Mostafa Izadi"
	picture = "IRCG_Gen_Mostafa_Izadi.dds"
	traits = { commando trait_engineer }
	skill = 3
}

create_corps_commander = {
	name = "Qasem Soleimani"
	picture = "IRCG_Gen_Qasem_Soleimani.dds"
	traits = { commando fortress_buster trickster urban_assault_specialist }
	skill = 5
}

create_corps_commander = {
	name = "Yahya Rahim Safavi"
	picture = "IRCG_Gen_Yahya_Rahim_Safavi.dds"
	traits = { commando fortress_buster swamp_fox }
	skill = 3
}


create_navy_leader = {
	name = "Ali Fadavi"
		picture = "IRCG_Navy_Ali_Fadavi.dds"
	traits = { blockade_runner }
	skill = 3
}

create_navy_leader = {
	name = "Habibollah Sayyari"
		picture = "navy_Habibollah_Sayyari.dds"
	traits = { seawolf }
	skill = 3
}
create_navy_leader = {
	name = "Ali Shamkani"
	picture = "Portrait_Ali_Shamkhani.dds"
	traits = { seawolf }
	skill = 1
}
