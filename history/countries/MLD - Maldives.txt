﻿capital = 281

oob = "MLD_2017"

set_research_slots = 2
set_convoys = 25
set_stability = 0.35

set_country_flag = country_language_dhivehi

set_technology = {

}

add_ideas = {

}

set_politics = {

	parties = {
		islamist = {
			popularity = 1
		}
		nationalist = {
			popularity = 1
		}
		reactionary = {
			popularity = 2
		}
		conservative = {
			popularity = 30
		}
		market_liberal = {
			popularity = 6
		}
		social_liberal = {
			popularity = 25
		}
		social_democrat = {
			popularity = 5
		}
		progressive = {
			popularity = 10
		}
		democratic_socialist = {
			popularity = 10
		}
		communist = {
			popularity = 10
		}
	}
	
	ruling_party = conservative
	last_election = "2013.11.16"
	election_frequency = 52
	elections_allowed = yes
}

create_country_leader = {
	name = "Abdulla Yameen"
	picture = "MLD_Abdulla_Yameen.dds"
	ideology = right_wing_conservative
	traits = {
		#
	}
}

create_field_marshal = {
	name = "Ahmed Shiyam"
	picture = "Portrait_Ahmed_Shiyam.dds"
	traits = { organisational_leader }
	skill = 2
}

create_field_marshal = {
	name = "Ahmed Shahid"
	picture = "Portrait_Ahmed_Shahid.dds"
	traits = { thorough_planner }
	skill = 1
}

create_corps_commander = {
	name = "Zakariyya Mansoor"
	picture = "Portrait_Zakariyya_Mansoor.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Ali Zuhair"
	picture = "Portrait_Ali_Zuhair.dds"
	traits = { naval_invader }
	skill = 1
}

create_corps_commander = {
	name = "Ahmed Areef"
	picture = "Portrait_Ahmed_Areef.dds"
	traits = { urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Abdul Raheem"
	picture = "Portrait_Abdul_Raheem.dds"
	traits = { fortress_buster }
	skill = 1
}

create_corps_commander = {
	name = "Ali Ihusaan"
	picture = "Portrait_Ali_Ihusaan.dds"
	traits = { commando }
	skill = 1
}

create_navy_leader = {
	name = "Mohamed Ibrahim"
	picture = "Portrait_Mohamed_Ibrahim.dds"
	traits = { blockade_runner }
	skill = 1
}