﻿capital = 323

oob = "NEP_2017"

set_research_slots = 3
set_convoys = 10
set_stability = 0.25

set_country_flag = country_language_nepali

set_technology = {

}

add_ideas = {
}

set_politics = {

	parties = {
		monarchist = {
			popularity = 19
		}
		nationalist = {
			popularity = 5
		}
		conservative = {
			popularity = 15
		}
		democratic_socialist = {
			popularity = 24
		}
		communist = {
			popularity = 37
		}
	}
	
	ruling_party = communist
	last_election = "2017.12.7"
	election_frequency = 42
	elections_allowed = yes	
	
}

create_country_leader = {
	name = "Sher Bahadur Deuba"
	picture = "MDT_Sher_Bahadur_Deuba.dds"
	ideology = leninist
}

create_field_marshal = {
	name = "Rajendra Chhetri"
	picture = "Portrait_Rajendra_Chhetri.dds"
	traits = { old_guard organisational_leader }
	skill = 4
}

create_field_marshal = {
	name = "Baldev Raj Mahat"
	picture = "Portrait_Baldev_Raj_Mahat.dds"
	traits = { offensive_doctrine }
	skill = 3
}

create_corps_commander = {
	name = "Dipak Prasad Bharati"
	picture = "Portrait_Dipak_Prasad_Bharati.dds"
	traits = { panzer_leader }
	skill = 2
}

create_corps_commander = {
	name = "Shamsher Thakurathi"
	picture = "Portrait_Shamsher_Thakurathi.dds"
	traits = { trait_engineer }
	skill = 2
}

create_corps_commander = {
	name = "Jagadish Chandra Pokharel"
	picture = "Portrait_Jagadish_Chandra.dds"
	traits = { hill_fighter }
	skill = 2
}

create_corps_commander = {
	name = "Dev Kumar Subedi"
	picture = "Portrait_Dev_Kumar_Subedi.dds"
	traits = { trait_mountaineer }
	skill = 2
}

create_corps_commander = {
	name = "Sarad Kumar Giri"
	picture = "Portrait_Sarad_Giri.dds"
	traits = { ranger }
	skill = 2
}

create_corps_commander = {
	name = "Prahlad Thapa"
	picture = "Portrait_Prahlad_Thapa.dds"
	traits = { fortress_buster }
	skill = 2
}

create_corps_commander = {
	name = "Jhankar Bahadur Kadayat"
	picture = "Portrait_Jhankar_Bahadur_Kadayat.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Hom Kumar Lawati"
	picture = "Portrait_Hom_Kumar_Lawati.dds"
	traits = {  }
	skill = 2
}