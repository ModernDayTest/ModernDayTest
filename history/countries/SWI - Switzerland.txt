﻿capital = 151

oob = "SWI_2017"

set_research_slots = 3
set_convoys = 10
set_stability = 0.85

set_country_flag = country_language_german
set_country_flag = country_language_french
set_country_flag = country_language_italian
set_country_flag = country_language_romansh

set_technology = {
}

add_ideas = {
}

give_guarantee = LIC

set_politics = {

	parties = {
		islamist = {
			popularity = 0
		}
		nationalist = {
			popularity = 2
		}
		reactionary = {
			popularity = 22.6
		}
		conservative = {
			popularity = 2
		}
		market_liberal = {
			popularity = 19.9
		}
		social_liberal = {
			popularity = 15.8
		}
		social_democrat = {
			popularity = 22.5
		}
		progressive = {
			popularity = 5
		}
		democratic_socialist = {
			popularity = 1
		}
		communist = {
			popularity = 0
		}
	}

	ruling_party = reactionary
	last_election = "1999.9.24"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Albert Rosti"
	picture = "albert_rosti.dds"
	ideology = counter_progressive_democrat
}

create_country_leader = {
	name = "Rudolf Keller"
	picture = "Rudolf_Keller.dds"
	ideology = national_democrat
}

create_country_leader = {
	name = "Johann Schneider-Ammann"
	picture = "Johann_Schneider-Ammann.dds"
	ideology = libertarian
}

create_country_leader = {
	name = "Doris Leuthard"
	picture = "Doris_Leuthard.dds"
	ideology = liberalist
}

create_country_leader = {
	name = "Alain Berset"
	picture = "Alain_Berset.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Dominic Luthard"
	picture = "Dominic_Luthard.dds"
	ideology = national_socialist 
}

create_country_leader = {
	name = "Josef Zisyadis"
	picture = "Josef_Zisyadis.dds"
	ideology = democratic_socialist_ideology 
}

create_country_leader = {
	name = "Marianne Huguenin"
	picture = "Marianne_Huguenin.dds"
	ideology = marxist 
}

create_country_leader = {
	name = "Ruth Genner"
	picture = "Ruth_Genner.dds"
	ideology = green 
}

create_field_marshal = {
	name = "Philippe Rebord"
	picture = "Portrait_Philippe_Rebord.dds"
	traits = { old_guard organisational_leader }
	skill = 1
}

create_field_marshal = {
	name = "André Blattmann"
	picture = "Portrait_Andre_Blattmann.dds"
	traits = { thorough_planner }
	skill = 1
}

create_corps_commander = {
	name = "Aldo C. Schellenberg"
	picture = "Portrait_Aldo_Schellenberg.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Jean-Paul Theler"
	picture = "Portrait_Jean-Paul_Theler.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Thomas Kaiser"
	picture = "Portrait_Thomas_Kaiser.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Daniel Keller"
	picture = "Portrait_Daniel_Keller.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Willy Brülisauer"
	picture = "Portrait_Willy_Bruelisauer.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Peter Baumgartner"
	picture = "Portrait_Peter_Baumgartner.dds"
	traits = { trait_mountaineer }
	skill = 1
}

create_corps_commander = {
	name = "Hans Schori"
	picture = "Portrait_Hans_Schori.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Laurent Michaud"
	picture = "Portrait_Laurent_Michaud.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Raynald Droz"
	picture = "Portrait_Raynald_Droz.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Alain Vuitel"
	picture = "Portrait_Alain_Vuitel.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Fredy Keller"
	picture = "Portrait_Fredy_Keller.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Hans Schatzmann"
	picture = "Portrait_Hans_Schatzmann.dds"
	traits = { urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Claude Meier"
	picture = "Portrait_Claude_Meier.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Franz Nager"
	picture = "Portrait_Franz_Nager.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Daniel Baumgartner"
	picture = "Portrait_Daniel_Baumgartner.dds"
	traits = { fortress_buster }
	skill = 1
}

create_corps_commander = {
	name = "René Wellinger"
	picture = "Portrait_Rene_Wellinger.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Yvon Langel"
	picture = "Portrait_Yvon_Langel.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Mathias Tüscher"
	picture = "Portrait_Mathias_Tuescher.dds"
	traits = { hill_fighter }
	skill = 1
}

create_corps_commander = {
	name = "Alexander Kohli"
	picture = "Portrait_Alexander_Kohli.dds"
	traits = { hill_fighter }
	skill = 1
}

create_corps_commander = {
	name = "Martin Vögeli"
	picture = "Portrait_Martin_Voegeli.dds"
	traits = { winter_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Maurizio Dattrino"
	picture = "Portrait_Maurizio_Dattrino.dds"
	traits = { trait_mountaineer }
	skill = 1
}

create_corps_commander = {
	name = "Erik Labara"
	picture = "Portrait_Erik_Labara.dds"
	traits = { trait_mountaineer }
	skill = 1
}

create_corps_commander = {
	name = "Roland Favre"
	picture = "Portrait_Roland_Favre.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Hans-Peter Walser"
	picture = "Portrait_Hans-Peter_Walser.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Lucas Caduff"
	picture = "Portrait_Lucas_Caduff.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Hans-Peter Kellerhals"
	picture = "Portrait_Hans-Peter_Kellerhals.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Dominique Andrey"
	picture = "Portrait_Dominique_Andrey.dds"
	traits = { trickster }
	skill = 1
}

2015.9.18 = {

	set_politics = {
		parties = {
			nationalist = {
				popularity = 1.2
			}
			reactionary = {
				popularity = 29.4
			}
			conservative = {
				popularity = 6
			}
			market_liberal = {
				popularity = 16.4
			}
			social_liberal = {
				popularity = 11.6
			}
			social_democrat = {
				popularity = 18.8
			}
			progressive = {
				popularity = 11.7
			}
			democratic_socialist = {
				popularity = 1
			}
		}

		ruling_party = social_democrat
		last_election = "2015.9.18"
		election_frequency = 48
		elections_allowed = yes
	}
}