﻿capital = 778

oob = "CAF_2000"

set_research_slots = 2
set_convoys = 10
set_stability = 0.1

set_country_flag = country_language_french
set_country_flag = country_language_sango

set_technology = {

}

add_ideas = {

}

set_politics = {

	parties = {
		islamist = {
			popularity = 3
		}
		nationalist = {
			popularity = 0
		}
		reactionary = {
			popularity = 0
		}
		conservative = {
			popularity = 21
		}
		market_liberal = {
			popularity = 3
		}
		social_liberal = {
			popularity = 11
		}
		social_democrat = {
			popularity = 6
		}
		progressive = {
			popularity = 4
		}
		democratic_socialist = {
			popularity = 52
		}
		communist = {
			popularity = 0
		}
	}
	
	ruling_party = democratic_socialist
	last_election = "1999.9.19"
	election_frequency = 72
	elections_allowed = yes
}

create_country_leader = {
	name = "Ange-Félix Patassé"
	picture = "Ange_Patasse.dds"
	ideology = democratic_socialist_ideology
}

create_country_leader = {
	name = "Andre Kolingba"
	picture = "Andre_Kolingba.dds"
	ideology = constitutionalist
}

create_country_leader = {
	name = "David Dacko"
	picture = "David_Dacko.dds"
	ideology = moderate
}

create_country_leader = {
	name = "Jean-Paul Ngoupandé"
	picture = "Jean_Ngoupande.dds"
	ideology = progressive_ideology
}

create_country_leader = {
	name = "Enoch Derant Lakoue"
	picture = "Enoch_Lakoue.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Charles Massi"
	picture = "Charles_Massi.dds"
	ideology = libertarian
}

create_country_leader = {
	name = "Michel Djotodia"
	picture = "Michel_Djotodia.dds"
	ideology = islamic_authoritarian
}

create_country_leader = {
	name = "Anicet-Georges Dologuélé"
	picture = "Anicet_Dologuele.dds"
	ideology = counter_progressive_democrat
}

create_field_marshal = {
	name = "Xavier Sylvestre Yangongo"
	picture = "Portrait_Xavier_Sylvestre_Yangongo.dds"
	traits = { organisational_leader }
	skill = 3
}

2013.1.1 = {
	set_party_name = {
		ideology = social_liberal
		name = CAF_social_liberal_party_IND
	}
}

2015.12.30 = {
	set_politics = {
		parties = {
			islamist = {
				popularity = 5
			}
			nationalist = {
				popularity = 0
			}
			reactionary = {
				popularity = 13
			}
			conservative = {
				popularity = 10
			}
			market_liberal = {
				popularity = 0
			}
			social_liberal = {
				popularity = 61
			}
			social_democrat = {
				popularity = 0
			}
			progressive = {
				popularity = 1
			}
			democratic_socialist = {
				popularity = 10
			}
			communist = {
				popularity = 0
			}
		}
		ruling_party = social_liberal
		last_election = "2015.12.30"
		election_frequency = 60
		elections_allowed = yes
	}
	create_country_leader = {
		name = "Faustin-Archange Touadéra"
		picture = "CAR_faustin_archange_touadera.dds"
		ideology = moderate
	}
	create_country_leader = {
		name = "Désiré Kolingba"
		picture = "Desire_Kolingba.dds"
		ideology = constitutionalist
	}
	create_country_leader = {
		name = "Martin Ziguélé"
		picture = "Martin_Ziguele.dds"
		ideology = democratic_socialist_ideology
	}
}