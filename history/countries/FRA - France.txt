﻿capital = 16

oob = "FRA_2017"

set_research_slots = 5
set_convoys = 75
set_stability = 0.5

set_war_support = 0.3

set_country_flag = country_language_french

set_technology = {
	
}

add_ideas = {

}

set_politics = {

	parties = {
		#National Front - Le Pen
		reactionary = {
			popularity = 22
		}
		#The Republicans - Fillon
		conservative = {
			popularity = 18
		}
		#Socialist: La France Insoumise - Mélenchon
		democratic_socialist = {
			popularity = 17
		}		
		#En Marche - Emmanuel Macron
		social_liberal = {
			popularity = 28
		}
		#Socialist Party - Hamon
		social_democrat = {
			popularity = 8
		}
		progressive = {
			popularity = 3
		}
		communist = {
			popularity = 4
		}
	}
	
	ruling_party = social_liberal
	last_election = "2017.6.23"
	election_frequency = 60
	elections_allowed = yes
}


create_country_leader = {
	name = "Benoit Hamon"
	picture = "Benoit_Hamon.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Francois Fillon"
	picture = "Cons_Francois_Fillon.dds"
	ideology = gaullist
}

create_country_leader = {
	name = "Marine Le Pen"
	picture = "Nationalist_1_Marine_Le_Pen.dds"
	ideology = counter_progressive_democrat
}

create_country_leader = {
	name = "Alain Soral"
	picture = "Alain_Soral.dds"
	ideology = national_socialist  
}

create_country_leader = {
	name = "Andre Gandillon"
	picture = "Andre_Gandillon.dds"
	ideology = national_democrat 
}

create_country_leader = {
	name = "Jean-Luc Melenchon"
	picture = "Jean_Luc_Melenchon.dds"
	ideology = democratic_socialist_ideology
}

create_country_leader = {
	name = "Marie Buffet"
	picture = "Marie_Buffet.dds"
	ideology = leninist
}

create_country_leader = {
	name = "Noel Mamere"
	picture = "Noel_Mamere.dds"
	ideology = green
}
	
create_country_leader = {
	name = "Louis XX"
	picture = "Louis_XX.dds"
	ideology = absolute_monarchist
}

create_country_leader = {
	name = "Christiane Taubira"
	picture = "Christiane_Taubira.dds"
	ideology = liberalist
}

create_field_marshal = {
	name = "Jean-Pierre Bosser"
		picture = "gen_Jean_Pierre_Bosser.dds"
	traits = {  }
	skill = 3
}
create_corps_commander = {
	name = "Bertrant Ract Madoux"
		picture = "gen_Bertrand_Ract_Madoux.dds"
	traits = {  }
	skill = 2
}
create_corps_commander = {
	name = "Herve Bizeul"
		picture = "gen_Herve_Bizeul.dds"
	traits = {  }
	skill = 2
}
create_corps_commander = {
	name = "Nicolas Casanova"
		picture = "gen_Nicolas_Casanova.dds"
	traits = {  }
	skill = 3
}
create_navy_leader = {
	name = "Christophe Prazuck"
		picture = "adm_Christophe_Prazuck.dds"
	traits = {  }
	skill = 4
}
create_navy_leader = {
	name = "Denis Beraud"
		picture = "adm_Denis_Beraud.dds"
	traits = {  }
	skill = 3
}
create_navy_leader = {
	name = "Marc de Briancon"
		picture = "adm_Marc_de_Briancon.dds"
	traits = {  }
	skill = 4
}
create_navy_leader = {
	name = "Pierre Francois Forissier"
		picture = "adm_Pierre_Francois_Forissier.dds"
	traits = {  }
	skill = 4
}
create_field_marshal = {
	name = "Pierre de Villiers"
	picture = "Portrait_Pierre_de_Villiers.dds"
	traits = { old_guard organisational_leader }
	skill = 4
}

create_field_marshal = {
	name = "Arnaud Sainte-Claire Deville"
	picture = "Portrait_Arnaud_Sainte-Claire_Deville.dds"
	traits = { defensive_doctrine }
	skill = 3
}

create_corps_commander = {
	name = "François Labuze"
	picture = "Portrait_Francois_Labuze.dds"
	traits = { panzer_leader }
	skill = 2
}

create_corps_commander = {
	name = "Franck Boudet"
	picture = "Portrait_Franck_Boudet.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Christophe Bizien"
	picture = "Portrait_Christophe_Bizien.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Loïc Girard"
	picture = "Portrait_Loic_Girard.dds"
	traits = { naval_invader }
	skill = 1
}

create_corps_commander = {
	name = "Pascal Ianni"
	picture = "Portrait_Pascal_Ianni.dds"
	traits = { naval_invader }
	skill = 1
}

create_corps_commander = {
	name = "Thomas Mollard"
	picture = "Portrait_Thomas_Mollard.dds"
	traits = { hill_fighter }
	skill = 1
}

create_corps_commander = {
	name = "Etienne du Peyroux"
	picture = "Portrait_Etienne_du_Peyroux.dds"
	traits = { naval_invader }
	skill = 1
}

create_corps_commander = {
	name = "François Régis Jaminet"
	picture = "Portrait_Francois_Regis_Jaminet.dds"
	traits = { naval_invader }
	skill = 1
}

create_corps_commander = {
	name = "Jean-Jacques Fatinet"
	picture = "Portrait_Jean-Jacques_Fatinet.dds"
	traits = { trait_mountaineer }
	skill = 1
}

create_corps_commander = {
	name = "Guillaume de Sercey"
	picture = "Portrait_Guillaume_de_Sercey.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Guillaume Venard"
	picture = "Portrait_Guillaume_Venard.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Benoit Durieux"
	picture = "Portrait_Benoit_Durieux.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Éric Bellot des Minières"
	picture = "Portrait_Eric_Bellot_des_Minieres.dds"
	traits = { commando urban_assault_specialist }
	skill = 2
}

create_corps_commander = {
	name = "Jean-Frédéric Lenoble"
	picture = "Portrait_Jean-Frederic_Lenoble.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Eric Maury"
	picture = "Portrait_Eric_Maury.dds"
	traits = { trickster }
	skill = 3
}

create_corps_commander = {
	name = "Jean-François Lafont Rapnouil"
	picture = "Portrait_Jean-Francois_Lafont_Rapnouil.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Pierre Liot de Nortbecourt"
	picture = "Portrait_Pierre_Liot_de_Nortbecourt.dds"
	traits = { commando winter_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Jean Maurin"
	picture = "Portrait_Jean_Maurin.dds"
	traits = { commando desert_fox jungle_rat }
	skill = 3
}

create_navy_leader = {
	name = "Bruno Thouvenin"
	picture = "Portrait_Bruno_Thouvenin.dds"
	traits = { spotter }
	skill = 2
}

create_navy_leader = {
	name = "François Rebourd"
	picture = "Portrait_Francois_Rebourd.dds"
	traits = { seawolf }
	skill = 2
}

create_navy_leader = {
	name = "Louis-Michel Guillaume"
	picture = "Portrait_Louis-Michel_Guillaume.dds"
	traits = { blockade_runner }
	skill = 3
}

create_navy_leader = {
	name = "Charles-Henri du Ché"
	picture = "Portrait_Charles-Henri_du_Che.dds"
	traits = { air_controller }
	skill = 3
}

create_navy_leader = {
	name = "Pascal Ausseur"
	picture = "Portrait_Pascal_Ausseurs.dds"
	traits = { seawolf }
	skill = 3
}

create_navy_leader = {
	name = "Emmanuel de Oliveira"
	picture = "Portrait_Emmanuel_de_Oliveira.dds"
	traits = { spotter }
	skill = 3
}

create_navy_leader = {
	name = "Jean-Baptiste Dupuis"
	picture = "Portrait_Jean-Baptiste_Dupuis.dds"
	traits = { blockade_runner }
	skill = 3
}


2018.1.1 = {
	set_party_name = {
		ideology = democratic_socialist
		long_name = FRA_democratic_socialist_party_FI_long
		name = FRA_democratic_socialist_party_FI
	}
	create_country_leader = {
		name = "Emmanuel Macron"
		picture = "rtk_emmanuel_macron.dds"
		ideology = liberalist
	}
	set_party_name = {
		ideology = social_liberal
		long_name = FRA_social_liberal_party_En_Marche_long
		name = FRA_social_liberal_party_En_Marche
	}
}